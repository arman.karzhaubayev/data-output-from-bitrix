<?php
class CKa1ra
{

    // объект CDBResult;
    public $ndlCDBResult = null;

    /**********************************************************************
     * Где лежит файл курс валют
     */
    private static $informerRatesFile = '/api/kursy.xml';
    private static $informerRatesUrl = '/rate/kursy.xml';

    /**
     * Где лежит файл погоды
     */
    private static $informerWeatherFile = '/api/weather.json';
    private static $informerWeatherUrl = '/weather.json';

    // Времея посленей модификации файла (по умолчанию: 60min)
    private function is_time_expired($filename, $min=60) {
        $date_now = strtotime(date('d.m.Y H:i:s'));
        $m = date("d.m.Y H:i:s", filemtime($filename));
        $date_modifyed = strtotime($m);
        return intval($date_now) - intval($date_modifyed) >= 60 * $min;
    }
    private function file_get_contents_curl($url) {
		$result = True;
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Устанавливаем параметр, чтобы curl возвращал данные, вместо того, чтобы выводить их в браузер.
		curl_setopt($ch, CURLOPT_TIMEOUT, 4); // время ожидания 4sec, после чего сессия закрывается
		curl_setopt($ch, CURLOPT_URL, $url);
	
		$data = curl_exec($ch);
		$http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if(curl_errno($ch) == 0 AND $http == 200) {
			$result = $data;
		} else {
			$result = False;
		}
		curl_close($ch);
	
		return $result;
	}
    private function update_resource_file($filename, $url)
    {
        if(self::is_time_expired($filename)) {
            $fh = fopen($filename, "w");
            $resource = self::file_get_contents_curl($url);
            if($resource) {
                fwrite($fh, $resource);
            }
            fclose($fh);
        }
        return $filename;
    }
    /**
     * Получаем путь к файлу от курс валют
     *
     * return string
     */
    public static function get_exchange_rate($currencies)
    {
        $result = [];
        $file = $_SERVER["DOCUMENT_ROOT"].self::$informerRatesFile;
        self::update_resource_file($file, self::$informerRatesUrl);
        if(file_exists($file)) {
            $kursy = simplexml_load_file($file);
            foreach($kursy->children() as $key => $channel) {
                foreach($channel->item as $item) {
                    if(in_array($item->title, $currencies)) {
                        $result["$item->title"] = [];
                        $result["$item->title"]["description"] = $item->description;
                        $result["$item->title"]["change"] = $item->change;
                        $result["$item->title"]["index"] = $item->index;
                    }
                }
            }
        }
        return $result;
    }
    /**
     * Получаем путь к файлу от погоды
     *
     * return string
     */
    public static function get_weather_info($city="нур-султан")
    {
        $result = [];
        $file = $_SERVER["DOCUMENT_ROOT"].self::$informerWeatherFile;
        self::update_resource_file($file, self::$informerWeatherUrl);
        if(file_exists($file)) {
            $weather_file = file_get_contents($file);
            $json = json_decode($weather_file, true);
            $result = $json["$city"];
        }
        return $result;
    }

	/**
     * Получаем записи youtube
     *
     * return string
     */
    public static function get_youtube_info($playlistId,$count = 5,$key)
    {
        $result = [];
        $url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=".$playlistId."&maxResults=".$count."&key=".$key;
        $video = file_get_contents($url);
        $json = json_decode($video, true);
        $result = $json['items'];

        return $result;
    }

    /**********************************************************************
     * Получаем список городов
     *
     * return array
     */
    public static function getInformerWeatherCities()
    {
        return [
            1 => [
                'name' => 'Нур-Султан',
                'yandex' => 'astana'
            ],
            2 => [
                'name' => 'Алматы',
                'yandex' => 'almaty'
            ],
            3 => [
                'name' => 'Шымкент',
                'yandex' => 'chimkent'
            ]
        ];
    }


    /**********************************************************************
     * формирование Фильтра
     */
    static function getPerson($lang = 'ru')
    {
        global $arPersons;

        if (!count($arPersons)) {
            $arIblockId = array('ru' => 48, 'kz' => 47, 'en' => 49);
            $res = CIBlockElement::GetList(
                Array('ACTIVE_FROM' => 'DESC'),
                Array(
                    "ACTIVE" => "Y"
                , "IBLOCK_ID" => $arIblockId[$lang]
                ),
                false,
                false,
                Array(
                    "ID", "IBLOCK_ID",
                    "PROPERTY_*",
                    "DETAIL_PAGE_URL",
                    "NAME", "ACTIVE_FROM",
                    "PREVIEW_PICTURE", "PREVIEW_TEXT",
                    "DETAIL_PICTURE", "DETAIL_TEXT"
                )
            );

            $counter = 0;

            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties();

                $arPersons[$arFields['ID']]['NAME'] = $arFields['NAME'];
                $arPersons[$arFields['ID']]['ATTR_SURNAME'] = $arProps['ATTR_SURNAME']['VALUE'];
                $arPersons[$arFields['ID']]['ATTR_NAME'] = $arProps['ATTR_NAME']['VALUE'];
                $arPersons[$arFields['ID']]['ATTR_PATRONYMIC'] = $arProps['ATTR_PATRONYMIC']['VALUE'];
                $arPersons[$arFields['ID']]['DETAIL_PICTURE'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
            }
        }

        return $arPersons;
    }


    /**********************************************************************
     * формирование Фильтра
     */
    static function getFormatedDate($date, $farmat, $lang = 'ru')
    {
        $result = '';
        if ($farmat == 'H:i') {
            if ($lang != 'en') {
                if (date('d.m.Y') == substr($date, 0, 10)) {
                    $result = substr($date, 11, 5);
                } else {
                    $result = substr($date, 0, 10);
                }
            } else {
                // переведем дату из Английского формата (MM/DD/YYYY) в наш формат (d.m.Y)
                $dateNew = FormatDate('d.m.Y H:i', MakeTimeStamp($date));
                if (date('d.m.Y') == substr($dateNew, 0, 10)) {
                    $result = substr($dateNew, 11, 5);
                } else {
                    $result = substr($dateNew, 0, 10);
                }
            }
        } elseif ($farmat == 'd.m.Y H:i') {
            $result = substr($date, 0, 16);
        }
        return $result;
    }

    /**********************************************************************
     * формирование Сортировка
     */
    function __makeSort($blockId, $key = 'default', $lang = 'ru', $params)
    {
        $result = Array();

        if ($key == 'readable') {
            $result = Array('SHOW_COUNTER' => 'DESC');
        } else {
            $result = Array('ACTIVE_FROM' => 'DESC');
        }

        return $result;
    }

    /**
     * формирование Фильтра
     */
    function __makeFilter($blockId, $key = 'default', $lang = 'ru', $params)
    {
        $additions = Array();

        if ($key == 'code') {
            $additions = Array(
                "CODE" => $params['code']
            );
        } elseif ($key == 'daterange') {
            $additions = Array(
                ">DATE_ACTIVE_FROM" => $params['startdate']
            , "<=DATE_ACTIVE_FROM" => $params['enddate']
            );
            // $additions["<=DATE_ACTIVE_FROM"] = $params['enddate'];
        } elseif ($key == 'more') {
            $additions = Array(
                ">DATE_ACTIVE_FROM" => $params['startdate']
            , "<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
            );
        } elseif ($key == 'less') {
            $additions = Array(
                "<=DATE_ACTIVE_FROM" => $params['startdate']
            );
        } elseif ($key == 'last') {
            $additions = Array(
                "<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
            );
        } elseif ($key == 'main') {
            $additions = Array(
                "PROPERTY_SHOW_ON_MAIN_FLAG" => 1,
                "<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
            );
        } elseif ($key == 'last_month') {   // за последний Месяц
            if ($lang != 'en') {
                $additions = Array(
                    ">ACTIVE_FROM" => date("d.m.Y H:i:s", strtotime("-1 month"))
                );
            } else {
                $additions = Array(
                    ">ACTIVE_FROM" => date("m/d/Y h:i:s A", strtotime("-1 month"))  // 09/12/2019 05:03 PM
                );
            }
        } else {
            $additions = Array(
                "<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
            );
        }

        return Array(
            "ACTIVE" => "Y"
        , "IBLOCK_ID" => $blockId
        , $additions
        );
    }

    /**
     * implode nested array
     */
    static function arrImplode($arr)
    {
        $result = "";
        if (is_array($arr)) {
            foreach ($arr as $value) {
                if (is_array($value)) {
                    $result .= self::arrImplode($value);
                } else {
                    $result .= $value;
                }
            }
        } else {
            if (!empty($arr))
                $result = $arr;
        }
        return $result;
    }

    /**
     * кеш компнентов
     */
    static function getCacheNews($blockId, $newsCount, $key, $lang, $params = array())
    {
        $cache_id = md5(serialize("iblock_id_" . $blockId . $newsCount . $key . $lang . self::arrImplode($params)));
        $cache_dir = "/tagged_getlist";

        $obCache = new CPHPCache;
        if ($obCache->InitCache(180, $cache_id, $cache_dir)) {
            $result = $obCache->GetVars();
        } elseif (CModule::IncludeModule("iblock") && $obCache->StartDataCache()) {
            $result = array();

            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);

            $result = CKa1ra::getNews($blockId, $newsCount, $key, $lang, $params);

            if ($result === false) {
                return array();
            }

            $CACHE_MANAGER->RegisterTag("iblock_id_" . $blockId . $newsCount . $key . $lang . self::arrImplode($params));
            $CACHE_MANAGER->EndTagCache();

            $obCache->EndDataCache($result);
        } else {
            $result = array();
        }
        return $result;
    }

    /**
     * получение Наименование раздела (скции) по его ID
     */
    private function __getSectionNameById($IBLOCK_SECTION_ID)
    {
        $result = null;
        $res = CIBlockSection::GetByID($IBLOCK_SECTION_ID);
        if ($ar_res = $res->GetNext())
            return $ar_res['NAME'];
        return false;
    }

    /**
     * подготовка Элемента массива по последовательностью ключей
     * Последовательности необходимо отделять ДВОЙНЫМ НИЖНИМ ПОДЧЕРКИВАНИЕМ (!!!)
     * Длина последовательности не имеет ограничений
     */
    private function __prepareColumnName($columnName, $element){
        $result = &$element;
        $chain = explode("__", $columnName);
        foreach ($chain as &$item) {
            $result = &$result[$item];
        }
        return $result;
    }
    /**
     * получение данных Сгруппированные по КОЛИЧЕСТВУ по определенному Полю
     * к примеру, есть Инфоблок у которого имеется дополнительное Свойство "Область" (location)
     * и необходимо получить данные Сгруппированные по "Областям".
     * Для этого на вход функции getDataGrouppedByColumn параметру $columnName подаем знаечение "PROPERTIES__location__VALUE"
     * что означает: Учесть значение VALUE свойство (PROPERTIES) наименованием location
     */
    static function getDataGrouppedByColumn($blockId, $newsCount, $key='default', $lang='ru', $params=array(), $columnName, $sortDirection = 'DESC') {
        $arResult = array();
        $datas = self::getNews($blockId, $newsCount, $key, $lang, $params);
        foreach ($datas as &$data) {
            $needle = self::__prepareColumnName($columnName, $data);
            if(isset($needle)){
                if($arResult[$needle]) {
                    $arResult[$needle] += 1;
                } else {
                    $arResult[$needle] = 1;
                }
            }
        }
        if($sortDirection == 'DESC')
            arsort($arResult);
        else
            asort($arResult);
        return $arResult;
    }

    /**
     * формирование Новостей
     */
    static function getNews($blockId, $newsCount, $key='default', $lang='ru', $params=array()) {
        global $ndlCDBResult;

        $arSort  	= Array('ACTIVE_FROM' => 'DESC');
        $arFilter 	= Array(
            "ACTIVE"    			=> "Y",
            "IBLOCK_ID" 			=> $blockId,
            "<=DATE_ACTIVE_FROM" 	=> date('d.m.Y H:i:s')
        );
        if (function_exists('makeSort')) {
            $arSort = makeSort($blockId, $key, $lang, $params);
        }
        if (function_exists('makeFilter')) {
            $arFilter = makeFilter($blockId, $key, $lang, $params);
        }
        $res        = CIBlockElement::GetList(
            $arSort,
            $arFilter,
            false,
            Array("nTopCount" => $newsCount),
            Array(
                "ID","IBLOCK_ID",
                "PROPERTY_*",
                "DETAIL_PAGE_URL",
                "NAME","ACTIVE_FROM",
                "CODE","IBLOCK_SECTION_ID",
                "PREVIEW_PICTURE","PREVIEW_TEXT",
                "DETAIL_PICTURE","DETAIL_TEXT",
                "SHOW_COUNTER", "TAGS", "SORT"
            )
        );

        $counter = 0;

        $ndlCDBResult = $res;
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arProps  = $ob->GetProperties();

            $result[$counter]['ID']             = $arFields['ID'];
            $result[$counter]['NAME']           = $arFields['NAME'];
            $result[$counter]['ACTIVE_FROM']    = $arFields['ACTIVE_FROM'];
            $result[$counter]['DATE_CREATE']    = $arFields['DATE_CREATE'];
            $result[$counter]['TIMESTAMP_X']    = $arFields['TIMESTAMP_X'];
            $result[$counter]['CODE']           = $arFields['CODE'];
            $result[$counter]['IBLOCK_SECTION_ID'] = $arFields['IBLOCK_SECTION_ID'];
            if(isset($arFields['IBLOCK_SECTION_ID'])){
                $result[$counter]['IBLOCK_SECTION_NAME'] = self::__getSectionNameById($arFields['IBLOCK_SECTION_ID']);
            }
            $result[$counter]['DETAIL_PAGE_URL'] = $arFields['DETAIL_PAGE_URL'];
            if (!empty($arFields['PREVIEW_PICTURE'])) {
                $result[$counter]['PREVIEW_PICTURE'] = CFile::GetPath($arFields['PREVIEW_PICTURE']);
                $result[$counter]['PREVIEW_PICTURE_SRC'] = $arFields['PREVIEW_PICTURE'];
            } else {
                $result[$counter]['PREVIEW_PICTURE'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
                $result[$counter]['PREVIEW_PICTURE_SRC'] = $arFields['DETAIL_PICTURE'];
            }
            $result[$counter]['PREVIEW_TEXT']   = $arFields['PREVIEW_TEXT'];
            $result[$counter]['DETAIL_PICTURE'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
            $result[$counter]['DETAIL_PICTURE_SRC'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
            $result[$counter]['DETAIL_PICTURE_ARRAY'] = CFile::GetFileArray($arFields['DETAIL_PICTURE']);
            $result[$counter]['DETAIL_TEXT']    = $arFields['DETAIL_TEXT'];
            $result[$counter]['SHOW_COUNTER']   = $arFields['SHOW_COUNTER'];

            $result[$counter]['is_parthner']    = isset($arProps['is_parthner']) && !empty($arProps['is_parthner']['VALUE']) ? true : false;
            $result[$counter]['is_photo']   	= isset($arProps['is_photo']) && !empty($arProps['is_photo']['VALUE']) ? true : false;
            $result[$counter]['is_video']   	= isset($arProps['is_video']) && !empty($arProps['is_video']['VALUE']) ? true : false;

            $result[$counter]['TAGS']           = $arFields['TAGS'];

            ++$counter;
        }
        return $result;
    }


    /**
     * формирование Поискового результата
     */
    static function getSearchResult($blockId, $newsCount, $key, $lang, $params = array()) {
        global $ndlCDBResult;
        $result     = array();

        CModule::IncludeModule('search');

        $query      = $params['query'];
        $SITE_ID    = 's1';

        $obSearch   = new CSearch;
        $obSearch->SetOptions(
            array(
                'ERROR_ON_EMPTY_STEM' => false, //добавили параметр, чтобы не ругался на форматирование запроса
            )
        );
        $obSearch->Search(
            array(
                'QUERY'      => $query,
                // 'SITE_ID'    => $SITE_ID,
                'MODULE_ID'  => 'iblock',
                'PARAM2'     => $blockId
            ),
            array('DATE_FROM' => 'DESC')
        );
        if (!$obSearch->selectedRowsCount()) {//и делаем резапрос, если не найдено с морфологией...
            $obSearch->Search(
                array(
                    'QUERY'   => $query,
                    // 'SITE_ID' => $SITE_ID,
                    'MODULE_ID' => 'iblock',
                    'PARAM2'  => $blockId
                ),
                array('DATE_FROM' => 'DESC'),
                array('STEMMING' => false) //... уже с отключенной морфологией
            );
        }

        $counter = 0;
        while ($row = $obSearch->fetch()) {
            $elem 								= self::getElementById($row['ITEM_ID']);
            $imgPath							= "";
            $showCounter 						= "";
            if($elem){
                $imgPath 						= CFile::GetPath($elem["PREVIEW_PICTURE"]);
                $showCounter 					= $elem['SHOW_COUNTER'];
            }

            $result[$counter]['ID']             = $row['ITEM_ID'];
            $result[$counter]['NAME']           = $row['TITLE'];
            $result[$counter]['ACTIVE_FROM']    = date('d.m.Y H:i:s', strtotime($row['DATE_FROM']));
            $result[$counter]['DETAIL_PAGE_URL'] = $row['URL'];
            $result[$counter]['DETAIL_TEXT']    = $row['BODY_FORMATED'];
            $result[$counter]['PREVIEW_PICTURE'] = $imgPath;
            $result[$counter]['SHOW_COUNTER']   = $showCounter;
            ++$counter;
            if($counter >= $newsCount)
                break;
        }
        return $result;
    }


    /**********************************************************************
    /**
     * увеличение количества просмотров SHOW_COUNTER
     */
    static function UpdateShowCounter($ID) {
        $itemElement = CIBlockElement::GetByID($ID)->Fetch();
        $SHOW_COUNTER = ($itemElement["SHOW_COUNTER"] == null ? 0 : $itemElement["SHOW_COUNTER"]) + 1;
//            echo $SHOW_COUNTER;

        $element = new CIBlockElement;
        $res = $element->Update($ID, array(
            "SHOW_COUNTER" => $SHOW_COUNTER
        ));
    }

    /**
     * получение XML файл
     *
     * return xml
     */
    public static function getXml()
    {
        // Получаем локальный файл
        $xml = Self::readXml();

        // Обновляем локальный файл для будущих людей
        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_time = 10800; // 3 часа
        $cache_id = 'getXml';
        $cache_path = 'ClassCKa1ra';

        if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)) {
            $result = $cache->GetVars();
        } else if($cache->StartDataCache()) {
            $url = GlobalConfigSite::getUrl() . '/api/informers/currency.php';

            $ch = curl_init();

            /*Опции запроса, чтобы не дожидаться ответа*/
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_exec($ch);
            curl_close($ch);

            $result = 1;
            $cache->EndDataCache($result);

            unset($url);
        }

        return $xml;
    }


    /**********************************************************************
     * добавление данных в JSON-файл
     */
    static function addDateToJson($fileName, $data){
        if(file_exists($fileName)){
            $string     = file_get_contents($fileName);
            $obj        = json_decode($string, true);
            array_push($obj, $data);
            $jsonData   = json_encode($obj);
            file_put_contents($fileName, $jsonData);
        } else {
            $fp         = fopen($fileName, 'w');
            fwrite($fp, json_encode($data));
            fclose($fp);
        }
    }

    /**
     * создание JSON-файла
     *
     * @data = array
     */
    public static function makeJsonFile($data){
        $file = $_SERVER['DOCUMENT_ROOT'] . Self::getInformerWeatherFile();

        $fp   = fopen($file, 'w');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }

    /**
     * получение JSON
     *
     * @isArray = boolean
     *
     * return object | array
     */
    public static function getJson($isArray = false)
    {
        $return = Self::readJson($isArray);

        // Обновляем локальный файл для будущих людей
        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_time = 3600; // 1 час
        $cache_id = 'getJson';
        $cache_path = 'ClassCKa1ra';

        if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)) {
            $result = $cache->GetVars();
        } else if($cache->StartDataCache()) {
            $url = GlobalConfigSite::getUrl() . '/api/informers/weather.php';

            $ch = curl_init();

            /*Опции запроса, чтобы не дожидаться ответа*/
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_exec($ch);
            curl_close($ch);

            $result = 1;
            $cache->EndDataCache($result);

            unset($url);
        }

        return $return;
    }

    /**
     * чтение JSON-файла
     *
     * @isArray = boolean
     *
     * object | array
     */
    public static function readJson($isArray = false)
    {
        $file = $_SERVER['DOCUMENT_ROOT'] . Self::getInformerWeatherFile();

        if (is_file($file)) {
            $obj = file_get_contents($file);
            $obj = json_decode($obj, $isArray);

            return $obj;
        }
        return false;
    }


    /**********************************************************************
     * Формирование Массива Данных по BLOCK_ID
     * @param integer $block_id
     * @return array
     */
    static function getMultiName($block_id) {
        $result = [];
        $arFilter = Array(
            "ACTIVE" => "Y"
        ,"IBLOCK_ID" => $block_id
        );
        $res = CIBlockElement::GetList(
            Array('ID' => 'ASC'),
            $arFilter,
            false,
            false,
            Array('ID','NAME')
        );
        while($ob = $res->GetNext()){
            $result[$ob['ID']] = trim($ob['NAME']);
        }
        return $result;
    }

    static function getMultiNameReverce($block_id) {
        $result = [];
        $arFilter = Array(
            "ACTIVE" => "Y"
        ,"IBLOCK_ID" => $block_id
        );
        $res = CIBlockElement::GetList(
            Array('ID' => 'ASC'),
            $arFilter,
            false,
            false,
            Array('ID','NAME')
        );
        while($ob = $res->GetNext()){
            $result[trim($ob['NAME'])] = $ob['ID'];
        }
        return $result;
    }


    /**********************************************************************
    /**
     * Так как IP локальный, соотвественно иконки не будут грузиться у кого нет VPN
     * Принято решение забирать с сервера на этот сервер
     *
     * @pach = string
     *
     * return string
     */
    static function getIcon($path) {
        $root = $_SERVER['DOCUMENT_ROOT'];
        if (is_file($root . $path) === false) {

            $array = explode('.', strtolower($path));
            $extension = end($array);
            unset($array);
            $extension_allow = array('svg', 'jpg', 'png');

            if (array_search($extension, $extension_allow) !== false) {

                $dir = str_replace('../', '', $path);
                $dir = explode('/', $path);
                array_pop($dir);
                $dir = implode('/', $dir);

                if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $dir)) {
                    mkdir($_SERVER['DOCUMENT_ROOT'] . $dir, 0777, true);
                }

                $fp = fopen ($root . $path, 'w+');
                $ch = curl_init(self::getServiceServer() . $path);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);

            }
        }
        return $path;
    }

    //**********************************************************************
    // NUMBER validation
    static function valid_number($str) {
        return (!preg_match("/^([0-9]+)$/", $str)) ? false : true;
    }
    // NAME validation
    static function valid_name($str) {
        return (!preg_match("/^([а-яА-ЯЁёa-zA-Z0-9\+_\-]+)$/iu", $str)) ? false : true;
    }
    // TEXT validation
    static function valid_text($str) {
        return (!preg_match("/^([әіңғүұқөһӘІҢҒҮҰҚӨҺа-яА-ЯЁёa-zA-Z0-9 \(\)\,\.\&\?\-\–\+\_\!\$\@\#\№\%\*\«\»\:\"\n])*$/iu", $str)) ? false : true;
    }
    // EMAIL validation
    static function valid_email($str) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? false : true;
    }
    // DATE validation FORMAT: 30.07.2019 13:58:02
    static function valid_date($str) {
        return (!preg_match("/([0-9]{2})\.([0-9]{2})\.([0-9]{4}) (?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]/", $str)) ? false : true;
    }
    // CELL-NOMBER validation FORMAT: +8(701) 120-8841
    static function valid_cell_phone($str) {
        return (!preg_match("/^(\+8|\+7)\(([0-9]{3})\) ([0-9]{3})-([0-9]{4})$/", $str)) ? false : true;
    }

    /**
     * колбэк функция для Комментарий
     * здесь можно реализовать Любую логику, к примеру, Отправка EMAIL админу...
     */
    static function callback_for_comment($params) {
        $result = true;
        return $result;
    }

    //
    static function getRealIP() {
        $ip = false;
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode (', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
            for ($i = 0; $i < count($ips); $i++) {
                if (!preg_match("/^(10|172\\.16|192\\.168)\\./", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }
};
?>
