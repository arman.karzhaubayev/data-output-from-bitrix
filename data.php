<?php
	define("NO_KEEP_STATISTIC", true); //Не учитываем статистику
	define("NOT_CHECK_PERMISSIONS", true); //Не учитываем права доступа
	// подключение Необходимых библиотек Битрикса
    require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if (!empty($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    header('Content-Type: application/json');

	use Bitrix\Main\Loader;
	Loader::includeModule("iblock");

    /**
	 * формирование Сортировка
	 */
	function makeSort($blockId, $key = 'default', $lang = 'ru', $params){
		$result = Array();

		if($key == 'readable'){
			$result = Array('SHOW_COUNTER' => 'DESC');
		} else {
			$result = Array('ACTIVE_FROM' => 'DESC');
		}

		return $result;
	}

	/**
	 * формирование Фильтра
	 */
	function makeFilter($blockId, $key = 'default', $lang = 'ru', $params){
		$additions = Array();

		if($key == 'daterange') {
			$additions = Array(
				">DATE_ACTIVE_FROM" 	=> $params['startdate']
				,"<=DATE_ACTIVE_FROM" 	=> $params['enddate']
			);
		} elseif($key == 'more') {
			$additions = Array(
				">DATE_ACTIVE_FROM" 	=> $params['startdate']
				,"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
			);
		} elseif($key == 'less') {
			$additions = Array(
				"<=DATE_ACTIVE_FROM" 	=> $params['startdate']
			);
		} elseif($key == 'last') {
			$additions = Array(
				"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
			);
		} elseif($key == 'main') {
			$additions = Array(
				"PROPERTY_SHOW_ON_MAIN_FLAG" => 1,
				"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
			);
		} elseif($key == 'readable') {   // за последний Месяц
			if($lang != 'en') {
				$additions = Array(
					">ACTIVE_FROM" => date("d.m.Y H:i:s", strtotime("-1 month"))
				);
			} else {
				$additions = Array(
					">ACTIVE_FROM" => date("m/d/Y h:i:s A", strtotime("-1 month"))  // 09/12/2019 05:03 PM
				);
			}
		} else {
			$additions = Array(
				"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
			);
		}

		return Array(
			"ACTIVE"    	=> "Y"
			,"IBLOCK_ID" 	=> $blockId
			,$additions
		);
	}

	/**
	 * формирование Новостей
	 */
	function getNews(
		$blockId, $newsCount, $key, $lang, $params,
		$getPreview = false, $getPhotoPreview = false,
		$getText = false, $getPhoto = false,
		$offset = 0
	){
		$arSort  	= Array('ACTIVE_FROM' => 'DESC');
		$arFilter 	= Array(
						"ACTIVE"    			=> "Y", 
						"IBLOCK_ID" 			=> $blockId, 
						"<=DATE_ACTIVE_FROM" 	=> date('d.m.Y H:i:s')
					);
		if (function_exists('makeSort')) {
			$arSort = makeSort($blockId, $key, $lang, $params);
		}
		if (function_exists('makeFilter')) {
			$arFilter = makeFilter($blockId, $key, $lang, $params);
		}

		if($key == 'offset') {
			$arNavStartParams["nTopCount"] = $newsCount+$offset;
		} else {
			$arNavStartParams["nTopCount"] = $newsCount;
		}

		$res        = CIBlockElement::GetList(
			$arSort,
			$arFilter,
			false,
			$arNavStartParams,
			Array(
				"ID","IBLOCK_ID",
				"PROPERTY_*",
				"DETAIL_PAGE_URL",
				"NAME","ACTIVE_FROM",
				"CODE","IBLOCK_SECTION_ID",
				"PREVIEW_PICTURE","PREVIEW_TEXT",
				"DETAIL_PICTURE","DETAIL_TEXT",
				"SHOW_COUNTER", "TAGS", "SORT"
			)
		);

		$counter = 0;
		$counterOffset = 0;
		$ndlCDBResult = $res;
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arProps  = $ob->GetProperties();

			if($key != 'offset') {
				$result[$counter]['ID']             = $arFields['ID'];
				$result[$counter]['NAME']           = $arFields['NAME'];
				if($params['keyIsMain']){
					$result[$counter]['ACTIVE_FROM'] = CKa1ra::getFormatedDate($arFields['ACTIVE_FROM'], 'H:i', $lang);
				} else {
					$result[$counter]['ACTIVE_FROM'] = $arFields['ACTIVE_FROM'];
				}
				$result[$counter]['DETAIL_PAGE_URL'] = $arFields['DETAIL_PAGE_URL'];
				if($getPhotoPreview){
					$result[$counter]['PREVIEW_PICTURE'] = CFile::GetPath($arFields['PREVIEW_PICTURE']);
					$result[$counter]['PREVIEW_PICTURE_SRC'] = $arFields['PREVIEW_PICTURE'];
				}
				if($getPreview){
					$result[$counter]['PREVIEW_TEXT']   = $arFields['PREVIEW_TEXT'];
				}
				if($getPhoto){
					$result[$counter]['DETAIL_PICTURE'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
					$result[$counter]['DETAIL_PICTURE_SRC'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
				}
				if($getText){
					$result[$counter]['DETAIL_TEXT']    = $arFields['DETAIL_TEXT'];
					$result[$counter]['SHOW_COUNTER']   = $arFields['SHOW_COUNTER'];
				}
			} else {
				if($counter < $offset){
					++$counter;
					continue;
				} else {
					$result[$counterOffset]['ID']             = $arFields['ID'];
					$result[$counterOffset]['NAME']           = $arFields['NAME'];
					if($params['keyIsMain']){
						$result[$counterOffset]['ACTIVE_FROM'] = CKa1ra::getFormatedDate($arFields['ACTIVE_FROM'], 'H:i', $lang);
					} else {
						$result[$counterOffset]['ACTIVE_FROM'] = $arFields['ACTIVE_FROM'];
					}
					$result[$counterOffset]['DETAIL_PAGE_URL'] = $arFields['DETAIL_PAGE_URL'];
					if($getPhotoPreview){
						$result[$counterOffset]['PREVIEW_PICTURE'] = CFile::GetPath($arFields['PREVIEW_PICTURE']);
						$result[$counterOffset]['PREVIEW_PICTURE_SRC'] = $arFields['PREVIEW_PICTURE'];
					}
					if($getPreview){
						$result[$counterOffset]['PREVIEW_TEXT']   = $arFields['PREVIEW_TEXT'];
					}
					if($getPhoto){
						$result[$counterOffset]['DETAIL_PICTURE'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
						$result[$counterOffset]['DETAIL_PICTURE_SRC'] = CFile::GetPath($arFields['DETAIL_PICTURE']);
					}
					if($getText){
						$result[$counterOffset]['DETAIL_TEXT']    = $arFields['DETAIL_TEXT'];
						$result[$counterOffset]['SHOW_COUNTER']   = $arFields['SHOW_COUNTER'];
					}
					++$counterOffset;
				}
			}
			++$counter;
		}
		return $result;
	}

	/*
		 k=default
		 i=1
		 c=5
		 lang=ru
		 dir=more
		 mobile=0
		 sdate='18.10.2019 00:00:00'
		 edate='18.10.2019 23:59:59'
		 param[one]=one
		 param[two]=two
	*/

	$preJson 		= array();
	$params			= array();
	$arKeys			= array(
						'default', 
						'daterange', 'more', 'less', 'last', 'main', 
						'author', 'theme', 'tag', 'city', 'offset'
					);

	$newsBlockId	= isset($_REQUEST['i']) ? intval($_REQUEST['i']) : 1; // ID info_block
	$newsCount		= isset($_REQUEST['c']) ? intval($_REQUEST['c']) : 15; // count
	$offset			= isset($_REQUEST['o']) ? intval($_REQUEST['o']) : 0;  // offset
	$lang 			= isset($_REQUEST['lang']) ? (in_array($_REQUEST['lang'], array('ru','kz','en')) ? $_REQUEST['lang'] : 'ru') : 'ru';
	$isMobile		= isset($_REQUEST['mobile']) ? (in_array($_REQUEST['mobile'], array(0,1)) && $_REQUEST['mobile'] == 1 ? true : false) : true;
	
	$params			= isset($_REQUEST['param']) ? $_REQUEST['param'] : array();
	
	$key 			= isset($_REQUEST['k']) ? (in_array($_REQUEST['k'], $arKeys) ? $_REQUEST['k'] : 'default') : 'default';
	$params['keyIsMain'] 	= false;
	if($key == 'main') {
		$params['keyIsMain'] = true;
	}

	$direction		= isset($_REQUEST['dir']) ? (in_array($_REQUEST['dir'], array('more','less')) ? $_REQUEST['dir'] : 'ru') : 'less';
	$params['dir'] 	= $direction;

	$startdate		= isset($_REQUEST['sdate']) ? (CKa1ra::valid_date($_REQUEST['sdate']) ? $_REQUEST['sdate'] : date('d.m.Y')." 00:00:00") : date('d.m.Y')." 00:00:00";
	$isStartDateUse = isset($_REQUEST['sdate']) ? true : flase;
	if($isStartDateUse){
		$params['startdate'] = $startdate;
	}
	$enddate		= isset($_REQUEST['edate']) ? (CKa1ra::valid_date($_REQUEST['edate']) ? $_REQUEST['edate'] : date('d.m.Y H:s:i')) : date('d.m.Y H:i:s');
	$isEnddateUse	= isset($_REQUEST['edate']) ? true : false;
	if($isEnddateUse){
		$params['enddate'] = $enddate;
	}

	// //request is ajax
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
		if($key == 'author'){
			$news = CKa1ra::getMultiName(17); // Авторы материалов
			$preJson['dictionary'] = $news;
		} elseif($key == 'theme'){
			$news = CKa1ra::getMultiName(9); // Тематические направления госзаказа
			$preJson['dictionary'] = $news;
		}  elseif($key == 'tag'){
			$news = CKa1ra::getMultiName(3); // Теги
			$preJson['dictionary'] = $news;
		}  elseif($key == 'city'){
			$news = CKa1ra::getMultiName(8); // Города
			$preJson['dictionary'] = $news;
		} else {
			$news = getNews($newsBlockId, $newsCount, $key, $lang, $params, false, false, false, false, $offset);
			$preJson['news'] = $news;
		}
		$preJson['status'] = true;
	} else {
		$preJson['status'] = false;
	}

	// JSON OUTPUT
	echo json_encode($preJson);
?> 
